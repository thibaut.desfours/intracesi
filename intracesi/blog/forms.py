from django import forms

class DateTimeInput(forms.DateTimeInput):
    input_type = 'datetime-local'

class EventForm(forms.Form):
    name = forms.CharField()
    dateStart = forms.DateTimeField(widget=DateTimeInput)
    dateEnd = forms.DateTimeField(widget=DateTimeInput)

    name.widget.attrs.update({'class': 'shadow appearance-none border rounded py-2 px-3 text-grey-darker', 'placeholder': 'Nom'})
    dateStart.widget.attrs.update({'class': 'shadow appearance-none border rounded py-2 px-3 text-grey-darker', 'placeholder': 'Date de début'})
    dateEnd.widget.attrs.update({'class': 'shadow appearance-none border rounded py-2 px-3 text-grey-darker', 'placeholder': 'Date de fin'})
