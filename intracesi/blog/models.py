from django.db import models

class Yeargroup(models.Model):
    name = models.TextField()
    year = models.TextField()

    def __unicode__(self):
        return self.name
"""
class Student(AbstractUser):
    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['username']

    email = models.EmailField(('email address'), unique=True)
    birthdate = models.DateField()
    yeargroup = models.ForeignKey(Yeargroup, on_delete=models.CASCADE)
    object = models.Manager()

    def __unicode__(self):
        return self.first_name
"""

class Student(models.Model):
    firstname = models.TextField()
    lastname = models.TextField()
    email = models.EmailField()
    birthdate = models.DateField()
    entreprise = models.TextField()
    yeargroup = models.ForeignKey(Yeargroup, on_delete=models.CASCADE)

    def __unicode__(self):
        return self.firstname

class Event(models.Model):
    name = models.TextField()
    dateStart = models.DateTimeField()
    dateEnd = models.DateTimeField()

    def __unicode__(self):
        return self.name

