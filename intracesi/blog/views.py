from django.shortcuts import render, redirect
from django.template import Context, loader
from blog.models import *
from django.http import HttpResponse, HttpResponseRedirect
from .forms import EventForm

def trombi(request, yeargroup_id):
    yeargroupP = Yeargroup.objects.get(id = yeargroup_id)
    students = Student.objects.filter(yeargroup = yeargroupP)
    return render(request, 'trombi.html', {'students' : students})

def profile(request, student_id):
    student = Student.objects.get(id = student_id)
    return render(request, 'profile.html', {'student' : student})

def yeargroups(request):
    yeargroups = Yeargroup.objects.all()
    return render(request, 'yeargroups.html', {'yeargroups' : yeargroups})

def events(request):
    events = Event.objects.all()
    form = EventForm(request.POST or None)

    if request.method == 'POST':
        form = EventForm(request.POST)
        if form.is_valid():
            event = Event()
            event.name = form.cleaned_data['name']
            event.dateStart = form.cleaned_data['dateStart']
            event.dateEnd = form.cleaned_data['dateEnd']
            event.save()

            return redirect(request.META['HTTP_REFERER'])

    return render(request, 'events.html', {'events' : events, 'form' : form})


